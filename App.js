import React from 'react';
import { SafeAreaView, StyleSheet, StatusBar } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import CityListComponent from './components/CityListComponent';
import WeatherComponent from './components/WeatherComponent';


const Stack = createNativeStackNavigator();

const App = () => {

  return (
    <SafeAreaView style={styles.container}>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="CityList"
            component={CityListComponent}
            options={{
              title: `Weather App - Today`,
              flex: 1,
              headerStyle: {
                backgroundColor: 'rgb(28, 91, 124)',
              },
              headerTitleAlign: 'center',
              headerTintColor: '#fff',
            }} />
          <Stack.Screen
            name="CityWeather"
            component={WeatherComponent}
            options={{
              title: 'Forecast',
              headerStyle: {
                backgroundColor: 'rgb(28, 91, 124)',
              },
              headerTintColor: '#fff',
              headerTitleAlign: 'center',
            }} />
        </Stack.Navigator>
      </NavigationContainer>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  }
});

export default App;