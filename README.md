# WeatherApp Project

## Available Scripts

### `expo start --web`

Open Expo cli in the web to preview of the application while in development mode. It will reload if you save edits to your files, and you will see build errors and logs in the terminal.

#### `expo start --ios` (not yet implemented)

Like `npm start`, but also attempts to open your app in the iOS Simulator if you're on a Mac and have it installed.

#### `expo start --android` (not yet implemented)

Like `npm start`, but also attempts to open your app on a connected Android device or emulator. Requires an installation of Android build tools (see [React Native docs](https://facebook.github.io/react-native/docs/getting-started.html) for detailed setup). We also recommend installing Genymotion as your Android emulator. Once you've finished setting up the native build environment, there are two options for making the right copy of `adb` available to Create React Native App:
