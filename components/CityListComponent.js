import React, { useState } from 'react';
import {
  SafeAreaView,
  View,
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  ImageBackground
} from 'react-native';

const CityListComponent = ({ navigation: { navigate } }) => {

  const [cities] = useState(require('./../assets/data/cities.json'));
  const weatherIcon = { uri: require('./../assets/icons/details.png') }
  const backgroundImage = { uri: "https://images.pexels.com/photos/3560044/pexels-photo-3560044.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" };

  return (
    <SafeAreaView style={styles.container}>
      <ImageBackground source={backgroundImage} resizeMode="cover" style={styles.backgroundImage}>
        <View style={styles.content}>
          <FlatList
            data={cities.cities}
            renderItem={({ item }) => (
              <TouchableOpacity style={styles.item} onPress={() => {
                navigate('CityWeather', item)
              }}>
                <Text style={styles.city}>{item.name}</Text>
                <ImageBackground style={styles.weatherIcon} source={weatherIcon} resizeMode="cover"></ImageBackground>
              </TouchableOpacity>
            )}
          />
        </View>
      </ImageBackground>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  backgroundImage: {
    width: '100%',
    height: '100%',
  },
  content: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  item: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'rgba(255, 255, 255, 0.75)',
    paddingVertical: 20,
    paddingHorizontal: 20,
    marginVertical: 8,
    marginHorizontal: 15,
    borderRadius: 15,
  },
  city: {
    paddingRight: 20,
    fontSize: 25,
    width: '90%',
    fontWeight: 'bold',
    color: 'rgb(28, 91, 124)'
  },
  weatherIcon: {
    width: 30,
    height: 30,
    alignItems: 'flex end',
  }
});

export default CityListComponent;