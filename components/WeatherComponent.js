import React, { useState, useEffect } from 'react';
import {
  SafeAreaView,
  View,
  FlatList,
  StyleSheet,
  Text,
  StatusBar,
  ImageBackground,
  ActivityIndicator
} from 'react-native';

const WeatherComponent = ({ route }) => {

  const [daysMonthsData] = useState(require('./../assets/data/date.json'));

  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  const apiUrl = `http://api.openweathermap.org/data/2.5/weather?q=${route.params.name}&units=metric&APPID=05804934ded1c0ea42ca1b7b8086724c`

  const getWeatherDetails = async () => {
    try {
      const response = await fetch(apiUrl);
      const json = await response.json();
      setData(json);
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    getWeatherDetails();
  }, []);

  var day = new Date().getDate();
  var month = new Date().getMonth();
  var dayOfWeek = daysMonthsData.days[new Date().getDay()];
  var currentMonth = daysMonthsData.months[month];
  var currentDate = dayOfWeek + ', ' + day + ' ' + currentMonth;

  const minIcon = { uri: require('./../assets/icons/minimum.png') };
  const maxIcon = { uri: require('./../assets/icons/maximum.png') };
  const windSpeedIcon = { uri: require('./../assets/icons/wind-speed.png') };
  const cloudsIcon = { uri: require('./../assets/icons/cloud.png') };
  const pressureIcon = { uri: require('./../assets/icons/pressure.png') };
  const humidityIcon = { uri: require('./../assets/icons/humidity.png') };

  return (
    <SafeAreaView style={styles.container}>
      <ImageBackground source={route.params.image} resizeMode="cover" style={styles.backgroundImage}>
        <View style={styles.content}>
          {isLoading ? <ActivityIndicator /> : (
            <FlatList
              data={[data]}
              keyExtractor={({ id }) => id}
              renderItem={({ item }) => {
                return (
                  <>
                    <Text style={styles.city}>{route.params.name}</Text>
                    <Text style={styles.date}>{currentDate}</Text>

                    <View style={styles.currentTemperatureWrapper}>
                      <View style={styles.currentTemperatureDetailWrapper}>
                        <Text style={styles.currentTemperature}>{parseInt(item.main.temp)}°</Text>
                        <ImageBackground
                          style={styles.weatherIcon}
                          source={`http://openweathermap.org/img/wn/${item.weather.map((icon) => icon.icon)}@2x.png`}
                          resizeMode="cover">
                        </ImageBackground>
                      </View>
                      <Text style={styles.feelTemperature}>Feels like {parseInt(item.main.feels_like)}°</Text>
                    </View>
                    <View style={styles.detailMainWrapperContainer}>

                      <View style={styles.detailContent}>
                        <Text style={styles.detailTitle}>Min</Text>
                        <View style={styles.detailWrapper}>
                          <ImageBackground style={styles.detailsIcon} source={minIcon} resizeMode="cover"></ImageBackground>
                          <Text style={styles.DetailText}>{parseInt(item.main.temp_min)}°</Text>
                        </View>
                      </View>

                      <View style={styles.detailContent}>
                        <Text style={styles.detailTitle}>Max</Text>
                        <View style={styles.detailWrapper}>
                          <ImageBackground style={styles.detailsIcon} source={maxIcon} resizeMode="cover"></ImageBackground>
                          <Text style={styles.DetailText}>{parseInt(item.main.temp_max)}°</Text>
                        </View>
                      </View>
                    </View>

                    <View style={styles.detailMainWrapperContainer}>
                      <View style={styles.detailContent}>
                        <Text style={styles.detailTitle}>Wind</Text>
                        <View style={styles.detailWrapper}>
                          <ImageBackground style={styles.detailsIcon} source={windSpeedIcon} resizeMode="cover"></ImageBackground>
                          <Text style={styles.DetailText}>{item.wind.speed} km/h</Text>
                        </View>
                      </View>

                      <View style={styles.detailContent}>
                        <Text style={styles.detailTitle}>Clouds</Text>
                        <View style={styles.detailWrapper}>
                          <ImageBackground style={styles.detailsIcon} source={cloudsIcon} resizeMode="cover"></ImageBackground>
                          <Text style={styles.DetailText}>{item.clouds.all}%</Text>
                        </View>
                      </View>
                    </View>

                    <View style={styles.detailMainWrapperContainer}>
                      <View style={styles.detailContent}>
                        <Text style={styles.detailTitle}>Pressure</Text>
                        <View style={styles.detailWrapper}>
                          <ImageBackground style={styles.detailsIcon} source={pressureIcon} resizeMode="cover"></ImageBackground>
                          <Text style={styles.DetailText}>{item.main.pressure} hPa</Text>
                        </View>
                      </View>

                      <View style={styles.detailContent}>
                        <Text style={styles.detailTitle}>Humidity</Text>
                        <View style={styles.detailWrapper}>
                          <ImageBackground style={styles.detailsIcon} source={humidityIcon} resizeMode="cover"></ImageBackground>
                          <Text style={styles.DetailText}>{item.main.humidity}%</Text>
                        </View>
                      </View>
                    </View>
                  </>
                )
              }}
            />
          )}
        </View>
      </ImageBackground>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },
  backgroundImage: {
    width: '100%',
    height: '100%',
  },
  content: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  city: {
    marginTop: 20,
    marginLeft: 20,
    fontSize: 50,
    fontWeight: 'bold',
    color: 'rgb(255, 255, 255)',
  },
  date: {
    marginLeft: 20,
    fontSize: 20,
    color: 'rgb(255, 255, 255)',
  },
  currentTemperatureWrapper: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  currentTemperature: {
    padding: 0,
    margin: 0,
    fontSize: 100,
    fontWeight: 'bold',
    color: 'rgb(255, 255, 255)',
  },
  weatherIcon: {
    width: 100,
    height: 100,
  },
  currentTemperatureDetailWrapper: {
    flexDirection: 'row',
  },
  feelTemperature: {
    fontSize: 30,
    color: 'rgb(255, 255, 255)',
  },
  detailMainWrapperContainer: {
    flexDirection: 'row',
    marginVertical: 10,
    marginHorizontal: 15,
    backgroundColor: 'rgba(255, 255, 255, 0.3)',
    borderRadius: 15
  },
  detailContent: {
    paddingTop: 10,
    paddingLeft: 20,
  },
  detailTitle: {
    fontSize: 20,
    color: 'rgb(255, 255, 255)',
  },
  detailWrapper: {
    flexDirection: 'row',
    paddingVertical: 20,
    width: 145
  },
  detailsIcon: {
    width: '30px',
    height: '30px',
  },
  DetailText: {
    marginLeft: 10,
    fontSize: 20,
    color: 'rgb(255, 255, 255)',
  }
});

export default WeatherComponent;